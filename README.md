# Understanding clouds from satellite images

This repository contains code, notebook and documentation for my solution to the Kaggle [Understanding Clouds from Satellite Images](https://www.kaggle.com/c/understanding_cloud_organization) competition.

## Getting started

I used a Google Deep Learning VM. Follow [these instructions](https://course.fast.ai/start_gcp.html) to setup one.

The `environment.yml` contains the Conda and Pip packages used for the project.

The data can be downloaded from Kaggle using the `kaggle-dl.sh` script.


