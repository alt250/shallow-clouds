#!/bin/bash

# requirement: conda install -c conda-forge kaggle

kaggle competitions download -p /kaggle/input/ understanding_cloud_organization

mkdir /kaggle/input/understanding_cloud_organization
cd /kaggle/input/understanding_cloud_organization

unzip ../understanding_cloud_organization.zip -d .

# fix extracted files permissions
chmod 644 *

mkdir train_images test_images
unzip -q train_images.zip -d train_images
unzip -q test_images.zip -d test_images

