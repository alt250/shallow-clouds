#!/bin/bash

# requirement: conda install -c conda-forge kaggle

# example usage:
#  ./kaggle-submit.sh submission.csv "$(git log -n1 --oneline)"

FILE=$1
MSG=$2

set -x
kaggle competitions submit -c understanding_cloud_organization -f ${FILE} -m "${MSG}"
set +x

echo 
